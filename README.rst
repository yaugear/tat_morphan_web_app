Web App for Tatar Morphological Analyser
========================================

Fork from (Vinilla Flask) https://bitbucket.org/yaugear/megaflask/src with py_tat_morphan python package(https://bitbucket.org/yaugear/py_tat_morphan) as Tatar Morphological Analyser.

To install and run:
-------------------
You will need flask web app and Tatar and Russian Morphological Analysers:

$ pip install flask flask-login flask-sqlalchemy sqlalchemy-migrate flask-wtf flask-restful pymysql

$ pip install pymorphy2 py_tat_morphan


then


$ git clone https://bitbucket.org/yaugear/tat_morphan_web_app

$ cd tat_morphan_web_app/mysite/

$ python run.py


or download dump.zip and restore it anywhere you want by typing

$ python restore.py

$ cd mysite/

$ python run.py



For feedback:
-------------

ramil.gata@gmail.com
