# -*- coding: UTF-8 -*-
"""
    Restfull API simples example.
    For more look here: 
        http://flask-restful-cn.readthedocs.io/en/0.3.4/quickstart.html

"""

import json

from flask_restful import Resource, Api, output_json

from app import app, MORPHAN

# class HelloWorld(Resource):
#     """
#         Example of class with REST methods
#     """
#     def get(self):
#         """
#             GET method of REST API
#         """
#         return {'hello': 'world'}, 200, {'Etag': 'some-opaque-string'}

#     def put(self):
#         """
#             PUT method of REST API
#         """
#         return '', 201

#     def delete(self):
#         """
#             DELETE method of REST API
#         """
#         return '', 404

class AnalyzeText(Resource):
    def get(self, text):
        """
            GET method of REST API
        """
        text = text[:1000]
        result = MORPHAN.process_text(text)
        return {'result': result}, 200, {'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*'}

    def put(self):
        """
            PUT method of REST API
        """
        return '', 404

    def delete(self):
        """
            DELETE method of REST API
        """
        return '', 404

class AnalyzeWord(Resource):
    def get(self, word):
        """
            GET method of REST API
        """
        parses = MORPHAN.analyse(word)
        if not parses:
            return {"parses": [{"parse": "NR", "pos": "NR", "lemma": "NR"},]},\
                   200, {'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*'}
        result = {"parses": []}
        for morph in parses.strip(';').split(';'):
            parts = morph.split('+')
            if len(parts) < 2:
                return {"parses": [{"parse": "NR", "pos": "NR", "lemma": "NR"},]},\
                       200, {'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*'}
            parse = {"parse": morph, "lemma": parts[0], "pos": parts[1]}
            result["parses"].append(parse)
        return result, 200, {'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*'}

    def put(self):
        """
            PUT method of REST API
        """
        return '', 404

    def delete(self):
        """
            DELETE method of REST API
        """
        return '', 404

api = Api(app)
api.add_resource(AnalyzeText, '/api/text/<string:text>')
api.add_resource(AnalyzeWord, '/api/word/<string:word>')
