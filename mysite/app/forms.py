# -*- coding: UTF-8 -*-
from flask.ext.wtf import Form
from wtforms import TextField, BooleanField, PasswordField
from wtforms.validators import Required, EqualTo, Email

from app.utils.validators import Unique
from .models import User

class LoginForm(Form):
    username = TextField('username', validators=[Required()])
    password = PasswordField('password', [Required()])
    remember_me = BooleanField('remember_me', default=True)

class RegistrationForm(Form):
    username = TextField('username', validators=[
        Required(),
        Unique(
            User,
            User.username,
            message='Такое имя пользователя уже занято!')
    ])
    password = PasswordField('password', [
        Required(),
        EqualTo('confirm', message='Пароли не совпадают.')
    ])
    confirm = PasswordField('Repeat Password')
    email = TextField('email', validators=[
        Required(),
        Email(),
        Unique(
            User,
            User.email,
            message='Пользователь с таким эмейлом уже зарегистрирован!')
    ])
