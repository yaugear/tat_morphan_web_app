# -*- coding: UTF-8 -*-
from flask import render_template, flash, redirect, url_for, request, g, jsonify, session, abort
from flask.ext.login import login_user, logout_user, current_user, login_required

from py_tat_morphan.morphan import is_amtype_pattern

from app import app, login_manager, db, MORPHAN
from app.forms import LoginForm, RegistrationForm
from app.models import *

@login_manager.user_loader
def load_user(id):
    """
        Used to get user by id
    """
    return User.query.get(int(id))

@app.before_request
def before_request():
    """
        Used to get current user
    """
    g.user = current_user

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    """
        Example of simple view
        This page will be available as main page ('/' or '/index')
        As result shows templates/index.html template
    """
    text = u'Язгы ташуларда көймә йөздерәбез.'
    result = MORPHAN.process_text(text)
    analysed = result.replace('\n', u'<br/>')
    return render_template("index.html",
                           title="TatMorphan",
                           user=g.user,
                           text=text,
                           analysed=analysed)

@app.route('/process_text', methods=['POST'])
def process_text():
    """
        Example of AJAX. 
        Server side gets name from POST request and returns 'Hello, %name%'
        FYI: for AJAX need to be callen as javascript function from HTML templates
        In this example, client side in templates/index.html
    """
    text = request.form['text'][:1000]
    # result = MORPHAN.process_text(text)

    if request.form['disam_style'] == '1':
        rules = Rule.query.filter(Rule.parent_id == None).filter(Rule.user_id == 1).all()
        disamrules = compile_rule(rules)
        sentences = MORPHAN.disambiguate_text(text, disamrules)
    elif request.form['disam_style'] == '2' and g.user and g.user.is_authenticated:
        rules = Rule.query.filter(Rule.parent_id == None).filter(Rule.user_id == g.user.id).all()
        disamrules = compile_rule(rules)
        sentences = MORPHAN.disambiguate_text(text, disamrules)
    else:
        sentences = MORPHAN.disambiguate_text(text)
    result = ''
    for sentence in sentences:
        for (word, chains) in sentence:
            result += '%s%s%s%s' % (word, "\n", chains, "\n")
    return jsonify({'result': result})

@app.route('/change_language/eng', methods=['GET'])
def change_language_english():
    session['lang'] = 'eng'
    return redirect(request.args.get('next', url_for('index')))

@app.route('/change_language/rus', methods=['GET'])
def change_language_russian():
    session['lang'] = 'rus'
    return redirect(request.args.get('next', url_for('index')))

@app.route('/change_language/tat', methods=['GET'])
def change_language_tatar():
    session['lang'] = 'tat'
    return redirect(request.args.get('next', url_for('index')))

@app.route('/morphan_tags', methods=['GET'])
def morphan_tags():
    """
        Morphological Tags infomation
    """
    return render_template("tags.html",
                           title="Morphan Tags",
                           user=g.user)

@app.route('/api/', methods=['GET'])
def api_info():
    """
        Short info about API
    """
    return render_template("api_info.html",
                           title="API",
                           user=g.user)

@app.route('/login', methods=['GET', 'POST'])
def login():
    """
        User login view
    """
    if g.user is not None:
        if g.user.is_authenticated:
            return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        username = request.form['username']
        password = request.form['password']
        remember_me = False
        if 'remember_me' in request.form:
            remember_me = True
        registered_user = User.query.filter_by(username=username, password=password).first()
        if registered_user is None:
            flash('Login or password are incorrect!', 'error')
            return redirect(url_for('login'))
        login_user(registered_user, remember=remember_me)
        flash('Success!')
        return redirect('/index')
    return render_template('login.html',
                           title='Login',
                           form=form)

@app.route('/logout')
def logout():
    """
        User logout view
    """
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    """
        User registration view
    """
    if g.user is not None:
        if g.user.is_authenticated:
            return redirect(url_for('index'))
    form = RegistrationForm()
    try:
        if form.validate_on_submit():
            user = User(request.form['username'], request.form['password'], request.form['email'])
            db.session.add(user)
            db.session.commit()
            flash('Success!')
            return redirect('/login')
    except:
        flash('Error!', 'error')
    return render_template('register.html',
                           title='Registration',
                           form=form,
                           user=g.user)

@app.errorhandler(404)
def page_not_found(e):
    """
        'Page not found' exception
    """
    return render_template('404.html',
                           user=g.user), 404

@app.errorhandler(403)
def page_not_found(e):
    """
        'Auth error' exception
    """
    return render_template('403.html',
                           user=g.user), 403

####################################################################################################

@app.route('/disambiguation', methods=['GET'])
def disam_info():
    """
        Morphological disambiguation infomation
    """
    return render_template("disam.html",
                           title="Morphological disambiguation",
                           user=g.user)

@app.route('/disambiguation/classes', methods=['GET'])
def disam_classes():
    """
        Morphological disambiguation infomation
    """
    return render_template("disam_classes.html",
                           title="FAQ",
                           user=g.user)

@app.route('/disambiguation/faq', methods=['GET'])
def disam_faq():
    """
        Morphological disambiguation infomation
    """
    return render_template("disam_faq.html",
                           title="FAQ",
                           user=g.user)

@app.route('/disambiguation/rules', methods=['GET', 'POST'])
def disam_rules():
    """
        Rules for morph disam
    """
    errors = []
    if g.user and g.user.is_authenticated:
        if request.method == 'POST':
            if request.form.get('rule_id'):
                # edit
                rule = Rule.query.get(request.form['rule_id'])
                if rule:
                    if is_amtype_pattern(request.form['rule_amtype_pattern'], errors):
                        rule.amtype_pattern = request.form['rule_amtype_pattern']
                        rule.parent_id = request.form['parent_id'] if request.form['parent_id'] and request.form['parent_id'] != rule.id else None
                        db.session.commit()
                        if 'submitcontinue' in request.form:
                            return redirect(url_for('disam_rule', rule_id=rule.id))
                    else:
                        for (errid, err) in errors:
                            flash(err)
                else:
                    abort(404)
            else:
                # add
                errors = []
                if is_amtype_pattern(request.form['rule_amtype_pattern'], errors):
                    rule = Rule(request.form['rule_amtype_pattern'],
                                request.form['parent_id'] if request.form['parent_id'] else None)
                    g.user.disam_rules.append(rule)
                    db.session.commit()
                    if 'submitcontinue' in request.form:
                        return redirect(url_for('disam_rule', rule_id = rule.id))
                else:
                    for (errid, err) in errors:
                        flash(err)
        rules = Rule.query.filter(Rule.parent_id == None).filter(Rule.user_id==g.user.id).all()
        allrules = Rule.query.filter(Rule.user_id==g.user.id).all()
    else:
        rules = Rule.query.filter(Rule.parent_id == None).filter(Rule.user_id==1).all()
        allrules = Rule.query.filter(Rule.user_id==1).all()
    return render_template("disam_rules.html",
                           title="Disam Rules",
                           rules=rules,
                           allrules=allrules,
                           errors=errors,
                           user=g.user)

@app.route('/disambiguation/rule/delete', methods=['POST'])
@login_required
def disam_rule_delete():
    if g.user and g.user.is_authenticated:
        rule = Rule.query.get(request.form['rule_id'])
        if rule:
            # delete all contexts and simple patterns
            for exception in rule.exceptions:
                exception.parent_id = rule.parent_id
            db.session.commit()
            for context in rule.contexts:
                for pattern in context.context_patterns:
                    db.session.delete(pattern)
                db.session.delete(context)
            db.session.delete(rule)
            db.session.commit()
            return jsonify({'result': 1})
    return jsonify({'result': 0})

@app.route('/disambiguation/context/delete', methods=['POST'])
@login_required
def disam_context_delete():
    if g.user and g.user.is_authenticated:
        context = Context.query.get(request.form['context_id'])
        if context:
            # delete all contexts and simple pattern
            for pattern in context.context_patterns:
                db.session.delete(pattern)
            db.session.delete(context)
            db.session.commit()
            return jsonify({'result': 1})
    return jsonify({'result': 0})

@app.route('/disambiguation/pullrequest', methods=['GET'])
@login_required
def disam_pullrequest():
    """
        Rules for morph disam
    """
    if not g.user or not g.user.is_authenticated or not g.user.username == 'admin':
        abort(404)
    rules = Rule.query.filter(Rule.status == 1).all()
    return render_template("disam_pullrequest.html",
                           title="Disam Rules Pull Request",
                           rules=rules,
                           user=g.user)

@app.route('/disambiguation/rule/pullrequest', methods=['POST'])
@login_required
def disam_rule_pullrequest():
    if g.user and g.user.is_authenticated:
        rule = Rule.query.get(request.form['rule_id'])
        if rule:
            rule.status = 1
            db.session.commit()
            return jsonify({'result': 1}), 200
        else:
            return jsonify({'result': 0}), 404
    return jsonify({'result': 0}), 403

def copy_rule(rule):
    newrule = Rule(rule.amtype_pattern)
    newrule.copyof = rule
    for context in rule.contexts:
        newcontext = Context(context.position, context.result_pattern)
        for pattern in context.context_patterns:
            newpattern = ContextPattern(pattern.position,
                                        pattern.pattern_type,
                                        pattern.searched,
                                        pattern.start_index,
                                        pattern.end_index,
                                        pattern.operand)
            newcontext.context_patterns.append(newpattern)
        newrule.contexts.append(newcontext)
    for exception in rule.exceptions:
        newexception = copy_rule(exception)
        newrule.exceptions.append(newexception)
    return newrule

@app.route('/disambiguation/rule/accept_request', methods=['POST'])
@login_required
def disam_rule_accept_request():
    if g.user and g.user.is_authenticated and g.user.username == 'admin':
        rule = Rule.query.get(request.form['rule_id'])
        if rule:
            rule.status = 0
            if len(rule.copies) > 0:
                db.session.delete(rule.copies[0])
            newrule = copy_rule(rule)
            if rule.parent and rule.parent.copies:
                newrule.parent = rule.parent.copies[0]
            g.user.disam_rules.append(newrule)
            db.session.commit()
            return jsonify({'result': 1}), 200
        else:
            return jsonify({'result': 0}), 404
    return jsonify({'result': 0}), 403

@app.route('/disambiguation/rule/<int:rule_id>', methods=['GET', 'POST'])
def disam_rule(rule_id):
    rule = Rule.query.get(rule_id)
    if not rule:
        abort(404)
    if rule.user != g.user and g.user.id != 1:
        abort(403)
    if g.user and g.user.is_authenticated and request.method == 'POST':
        if 'context_result_pattern' in request.form:
            if request.form.get('context_id'):
                # edit
                context = Context.query.get(request.form['context_id'])
                if context:
                    context.result_pattern = request.form['context_result_pattern']
                    db.session.commit()
                else:
                    # flash('Ошибка', 'error')
                    pass
            else:
                # add
                position = len(rule.contexts.all())
                context = Context(position, request.form['context_result_pattern'])
                rule.contexts.append(context)
                db.session.commit()
        elif 'context_up' in request.form:
            context = Context.query.get(request.form['context_id'])
            if context:
                context_after = Context.query.filter(Context.rule_id == context.rule_id)\
                                             .filter(Context.position < context.position)\
                                             .order_by(-Context.position).first()
                if context_after:
                    newpostion = context_after.position
                    context_after.position = context.position
                    context.position = newpostion
                    db.session.commit()
        elif 'context_down' in request.form:
            context = Context.query.get(request.form['context_id'])
            if context:
                context_after = Context.query.filter(Context.rule_id == context.rule_id)\
                                             .filter(Context.position > context.position)\
                                             .order_by(Context.position).first()
                if context_after:
                    newpostion = context_after.position
                    context_after.position = context.position
                    context.position = newpostion
                    db.session.commit()
        elif 'context_pattern_up' in request.form:
            context_pattern = ContextPattern.query.get(request.form['context_pattern_id'])
            if context_pattern:
                context_pattern_after = ContextPattern.query.filter(ContextPattern.context_id == context_pattern.context_id)\
                                                            .filter(ContextPattern.position < context_pattern.position)\
                                                            .order_by(-ContextPattern.position).first()
                if context_pattern_after:
                    newpostion = context_pattern_after.position
                    context_pattern_after.position = context_pattern.position
                    context_pattern.position = newpostion
                    db.session.commit()
        elif 'context_pattern_down' in request.form:
            context_pattern = ContextPattern.query.get(request.form['context_pattern_id'])
            if context_pattern:
                context_pattern_after = ContextPattern.query.filter(ContextPattern.context_id == context_pattern.context_id)\
                                                            .filter(ContextPattern.position > context_pattern.position)\
                                                            .order_by(ContextPattern.position).first()
                if context_pattern_after:
                    newpostion = context_pattern_after.position
                    context_pattern_after.position = context_pattern.position
                    context_pattern.position = newpostion
                    db.session.commit()
        elif 'context_pattern_id' in request.form:
            if request.form['context_pattern_id']:
                # edit
                context_pattern = ContextPattern.query.get(request.form['context_pattern_id'])
                if context_pattern:
                    context_pattern.pattern_type = request.form['pattern_type']
                    context_pattern.searched = request.form['searched'].strip(';') \
                                               if request.form['searched'] != ';'  \
                                               else request.form['searched']
                    context_pattern.start_index = request.form['start_index']
                    context_pattern.end_index = request.form['end_index']
                    context_pattern.operand = request.form['operand']
                    db.session.commit()
            else:
                #add
                context = Context.query.get(request.form['context_id'])
                if context:
                    position = len(context.context_patterns.all())
                    context_pattern = ContextPattern(position,
                                                     request.form['pattern_type'],
                                                     request.form['searched']\
                                                     if request.form['searched'] != ';'  \
                                                     else request.form['searched'],
                                                     request.form['start_index'],
                                                     request.form['end_index'],
                                                     request.form['operand']
                                                    )
                    context.context_patterns.append(context_pattern)
                    db.session.commit()
    return render_template("disam_rule.html",
                           title="Disam Rules",
                           rule=rule,
                           user=g.user)

@app.route('/disambiguation/context_pattern/delete', methods=['POST'])
@login_required
def disam_context_pattern_delete():
    if g.user and g.user.is_authenticated:
        context_pattern = ContextPattern.query.get(request.form['context_pattern_id'])
        if context_pattern:
            db.session.delete(context_pattern)
            db.session.commit()
            return jsonify({'result': 1})
    return jsonify({'result': 0})

def compile_rule(rules):
    result = []
    for rule in rules:
        contexts = []
        for context in rule.contexts:
            patterns = []
            for pattern in context.context_patterns:
                if pattern.operand == 0:
                    operand = 'and'
                elif pattern.operand == 1:
                    operand = 'or'
                elif pattern.operand == 2:
                    operand = 'and not'
                elif pattern.operand == 3:
                    operand = 'or not'
                else:
                    operand = ''
                patterns.append([pattern.pattern_type, 
                                 pattern.searched, 
                                 pattern.start_index, 
                                 pattern.end_index, 
                                 operand])
            contexts.append([patterns, context.result_pattern])
        result.append([rule.amtype_pattern, contexts, compile_rule(rule.exceptions)])
    return result

@app.route('/disambiguation/rules/get_as_json', methods=['GET'])
def get_disam_rules_as_json():
    if g.user and g.user.is_authenticated:
        user_id = g.user.id
    else:
        user_id = 1
    rules = Rule.query.filter(Rule.parent_id == None).filter(Rule.user_id == user_id).all()
    return jsonify(compile_rule(rules))
