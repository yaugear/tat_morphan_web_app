# -*- coding: UTF-8 -*-
from app import db

class User(db.Model):
    __tablename__ = "users"
    id = db.Column('user_id',db.Integer, primary_key=True)
    username = db.Column('username', db.String(20), unique=True, index=True)
    password = db.Column('password', db.String(10))
    email = db.Column('email', db.String(50), unique=True, index=True)
    disam_rules = db.relationship('Rule', backref='user', lazy='dynamic')

    def __init__(self , username ,password , email):
        self.username = username
        self.password = password
        self.email = email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __repr__(self):
        return '<User %r>' % (self.username)

class Rule(db.Model):
    __tablename__ = "rules"
    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('rules.id'))
    copyof_id = db.Column(db.Integer, db.ForeignKey('rules.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    amtype_pattern = db.Column(db.String(100))
    status = db.Column(db.Integer)
    copyof = db.relation('Rule', 
                         primaryjoin="Rule.id==Rule.copyof_id", 
                         remote_side=[id], 
                         backref="copies")
    parent = db.relation('Rule', 
                         primaryjoin="Rule.id==Rule.parent_id", 
                         remote_side=[id], 
                         backref="exceptions")
    contexts = db.relationship('Context', 
                               backref='rule', 
                               lazy='dynamic', 
                               order_by="Context.position")

    def __init__(self, amtype_pattern, parent_id=None):
        self.amtype_pattern = amtype_pattern
        self.parent_id = parent_id
        self.copyof = None
        self.status = 0

    def __repr__(self):
        return '<Disam Rule for %r>' % (self.amtype_pattern) 

class Context(db.Model):
    __tablename__ = "contexts"
    id = db.Column(db.Integer, primary_key=True)
    rule_id = db.Column(db.Integer, db.ForeignKey('rules.id'))
    position = db.Column(db.Integer)
    result_pattern = db.Column(db.String(100))
    context_patterns = db.relationship('ContextPattern', backref='context', lazy='dynamic', 
                                       order_by="ContextPattern.position")

    def __init__(self, position, result_pattern):
        self.result_pattern = result_pattern
        self.position = position

    def __repr__(self):
        return '<Contextual Rule disambiguate amtype{%s} as chain{%s}>'\
               % (self.rule.amtype_pattern, self.result_pattern) 

class ContextPattern(db.Model):
    __tablename__ = "context_patterns"
    id = db.Column(db.Integer, primary_key=True)
    context_id = db.Column(db.Integer, db.ForeignKey('contexts.id'))
    position = db.Column(db.Integer)
    pattern_type = db.Column(db.Integer)
    searched = db.Column(db.String(200))
    start_index = db.Column(db.Integer)
    end_index = db.Column(db.Integer)
    operand = db.Column(db.Integer)

    def __init__(self, position, pattern_type, searched, start_index, end_index, operand):
        self.position = position
        self.pattern_type = pattern_type
        self.searched = searched
        self.start_index = start_index
        self.end_index = end_index
        self.operand = operand

    def __repr__(self):
        return '<Context Pattern: Searches "%s" between %s and %s indexes>'\
               % (self.searched, self.start_index, self.end_index) 

